/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50624
Source Host           : localhost:3306
Source Database       : sandbox

Target Server Type    : MYSQL
Target Server Version : 50624
File Encoding         : 65001

Date: 2016-04-17 19:59:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for event
-- ----------------------------
DROP TABLE IF EXISTS `event`;
CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `del_flag` int(11) DEFAULT NULL,
  `place` varchar(255) DEFAULT '' COMMENT '活动地址',
  `start_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of event
-- ----------------------------
INSERT INTO `event` VALUES ('1', '北京大学测试活动', '2016-04-01 16:03:23', '0', '第十八教学楼5层801教室', '2016-04-05 12:34:36');
INSERT INTO `event` VALUES ('2', '清华附中测试活动', '2016-04-02 16:03:58', '1', '第十八教学楼5层802教室', '2016-04-06 12:34:39');
INSERT INTO `event` VALUES ('3', '清华附中测试活动', '2016-04-02 16:03:58', '0', '第十八教学楼5层802教室', '2016-04-06 12:34:39');
INSERT INTO `event` VALUES ('4', '清华附中测试活动', '2016-04-02 16:03:58', '0', '第十八教学楼5层802教室', '2016-04-06 12:34:39');
INSERT INTO `event` VALUES ('5', '清华附中测试活动', '2016-04-02 16:03:58', '0', '第十八教学楼5层802教室', '2016-04-06 12:34:39');
INSERT INTO `event` VALUES ('6', '清华附中测试活动', '2016-04-02 16:03:58', '1', '第十八教学楼5层802教室', '2016-04-06 12:34:39');
INSERT INTO `event` VALUES ('7', '清华附中测试活动', '2016-04-02 16:03:58', '0', '第十八教学楼5层802教室', '2016-04-06 12:34:39');
INSERT INTO `event` VALUES ('8', '清华附中测试活动', '2016-04-02 16:03:58', '0', '第十八教学楼5层802教室', '2016-04-06 12:34:39');
INSERT INTO `event` VALUES ('9', '清华附中测试活动', '2016-04-02 16:03:58', '1', '第十八教学楼5层802教室', '2016-04-06 12:34:39');
INSERT INTO `event` VALUES ('10', '清华附中测试活动', '2016-04-02 16:03:58', '0', '第十八教学楼5层802教室', '2016-04-06 12:34:39');
INSERT INTO `event` VALUES ('11', '热带旅行', '2016-04-05 20:53:24', '0', '三亚', '2016-04-29 00:00:00');
INSERT INTO `event` VALUES ('12', '测试活动2', '2016-04-05 20:54:14', '0', '北京四中', '2016-04-30 00:00:00');
INSERT INTO `event` VALUES ('13', '123', '2016-04-05 20:54:14', '1', '123', '2016-04-22 00:00:00');

-- ----------------------------
-- Table structure for event_user
-- ----------------------------
DROP TABLE IF EXISTS `event_user`;
CREATE TABLE `event_user` (
  `eid` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  KEY `eid` (`eid`),
  KEY `uid` (`uid`),
  CONSTRAINT `event_user_ibfk_1` FOREIGN KEY (`eid`) REFERENCES `event` (`id`),
  CONSTRAINT `event_user_ibfk_2` FOREIGN KEY (`uid`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of event_user
-- ----------------------------
INSERT INTO `event_user` VALUES ('1', '1');

-- ----------------------------
-- Table structure for financial
-- ----------------------------
DROP TABLE IF EXISTS `financial`;
CREATE TABLE `financial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `beginning_currency` decimal(10,0) DEFAULT NULL,
  `tax` decimal(10,0) DEFAULT NULL,
  `long_term_loan` decimal(10,0) DEFAULT NULL,
  `market_develop` decimal(10,0) DEFAULT NULL,
  `competitive_bid` decimal(10,0) DEFAULT NULL,
  `short_term_loan` decimal(10,0) DEFAULT NULL,
  `materials_fee` decimal(10,0) DEFAULT NULL,
  `sell_eqp` decimal(10,0) DEFAULT NULL,
  `buy_eqp` decimal(10,0) DEFAULT NULL,
  `production` decimal(10,0) DEFAULT NULL,
  `delivery` decimal(10,0) DEFAULT NULL,
  `product_development` decimal(10,0) DEFAULT NULL,
  `administration_fee` decimal(10,0) DEFAULT NULL,
  `others` decimal(10,0) DEFAULT NULL,
  `pay_short_interest` decimal(10,0) DEFAULT NULL,
  `short_loan_repay` decimal(10,0) DEFAULT NULL,
  `pay_long_interest` decimal(10,0) DEFAULT NULL,
  `depreciation` decimal(10,0) DEFAULT NULL,
  `total_income` decimal(10,0) DEFAULT NULL,
  `total_expense` decimal(10,0) DEFAULT NULL,
  `ending_currency` decimal(10,0) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `fyear` int(2) DEFAULT '1' COMMENT '第几财年',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `financial_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of financial
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) DEFAULT NULL,
  `password` varchar(80) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `del_flag` int(1) DEFAULT '0',
  `role` int(1) DEFAULT '0' COMMENT '0:用户，1：管理员',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000005 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'Tom', '123', '2016-03-07 20:55:10', '0', '0');
INSERT INTO `user` VALUES ('2', 'Tom2', '123', '2016-03-14 20:55:14', '1', '0');
INSERT INTO `user` VALUES ('999999', 'Admin', '123', '2016-04-02 12:26:33', '0', '1');
INSERT INTO `user` VALUES ('1000000', '咚霸歘', '123456', '2016-04-05 21:06:03', '0', null);
INSERT INTO `user` VALUES ('1000001', '1', '1', '2016-04-05 21:06:03', '1', null);
INSERT INTO `user` VALUES ('1000002', '第一小组', '123', '2016-04-05 21:14:19', '0', null);
INSERT INTO `user` VALUES ('1000003', '第二小组', '123', '2016-04-05 21:15:16', '0', null);
INSERT INTO `user` VALUES ('1000004', '第三小组', '123', '2016-04-05 21:16:46', '0', null);
