#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Wu Jin'

from flask.ext.wtf import Form
from wtforms import StringField, PasswordField, BooleanField, SubmitField,DateField
from wtforms.validators import DataRequired, Length, Email


class LoginForm(Form):
    id = StringField('id', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])


class CreateEventForm(Form):
    # id = StringField('活动编号')
    name = StringField('活动名称', validators=[DataRequired()])
    place = StringField('活动地点')
    start_date = DateField('活动日期')
    submit = SubmitField("提交")


class CreateUserForm(Form):
    # id = StringField('活动编号')
    name = StringField('公司名称', validators=[DataRequired()])
    password = PasswordField('登录密码', validators=[DataRequired()])
    submit = SubmitField("提交")
