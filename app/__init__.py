#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Wu Jin'

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager
from flask.ext.openid import OpenID
from flask_bootstrap import Bootstrap

from config import DATABASE_URI,SECRET_KEY


app = Flask(__name__, static_folder='static', template_folder='templates')
db = SQLAlchemy(app)
Bootstrap(app)
login_manager = LoginManager()
login_manager.login_view = '/login'
login_manager.init_app(app)
oid = OpenID()

app.config["SQLALCHEMY_DATABASE_URI"] = DATABASE_URI
app.config["SECRET_KEY"] = SECRET_KEY

@login_manager.user_loader
def load_user(user_id):
    from model import User
    return User.query.get(int(user_id))

def create_table(db):
    from model import User,Financial,Event
    db.create_all()

def dispatch_apps(app):
    # create_table(db)
    from app.views import sandbox,backend
    app.register_blueprint(sandbox)
    app.register_blueprint(backend,url_prefix="/admin")