# -*- encoding:utf-8 -*-
# Author Wu Jin
from app import db
from sqlalchemy_jsonapi import JSONAPI
from datetime import datetime
from flask.ext.login import UserMixin

event_user = db.Table("event_user",
                        db.Column("eid",db.Integer,db.ForeignKey('event.id')),
                        db.Column("uid",db.Integer,db.ForeignKey('user.id'))
                      )

class User(db.Model, UserMixin):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    password = db.Column(db.String(80))
    role = db.Column(db.Integer)
    create_date = db.Column(db.DATETIME, default=datetime.now())
    del_flag = db.Column(db.Integer, default=0)
    events = db.relationship("Event",secondary = event_user,backref=db.backref("users",lazy="dynamic"))



class Event(db.Model):
    __tablename__ = 'event'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    place = db.Column(db.String(255))
    start_date = db.Column(db.DATETIME, default=datetime.now())
    create_date = db.Column(db.DATETIME, default=datetime.now())
    del_flag = db.Column(db.Integer, default=0)

    def __init__(self, name=None):
        self.name = name


class Financial(db.Model):
    __tablename__ = 'financial'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User', backref=db.backref('financials', lazy='dynamic'))
    event_id = db.Column(db.Integer, db.ForeignKey('event.id'))
    event = db.relationship('Event', backref=db.backref('financials', lazy='dynamic'))
    beginning_currency = db.Column(db.DECIMAL)
    tax = db.Column(db.DECIMAL)
    long_term_loan = db.Column(db.DECIMAL)
    market_develop = db.Column(db.DECIMAL)
    competitive_bid = db.Column(db.DECIMAL)
    short_term_loan = db.Column(db.DECIMAL)
    materials_fee = db.Column(db.DECIMAL)
    sell_eqp = db.Column(db.DECIMAL)
    buy_eqp = db.Column(db.DECIMAL)
    production = db.Column(db.DECIMAL)
    delivery = db.Column(db.DECIMAL)
    product_development = db.Column(db.DECIMAL)
    administration_fee = db.Column(db.DECIMAL)
    others = db.Column(db.DECIMAL)
    pay_short_interest = db.Column(db.DECIMAL)
    short_loan_repay = db.Column(db.DECIMAL)
    pay_long_interest = db.Column(db.DECIMAL)
    depreciation = db.Column(db.DECIMAL)
    total_income = db.Column(db.DECIMAL)
    total_expense = db.Column(db.DECIMAL)
    ending_currency = db.Column(db.DECIMAL)
    create_date = db.Column(db.DATETIME, default=datetime.now())


    def __repr__(self):
        return '<Financial %r>' % self.id


serializer = JSONAPI(db.Model)
