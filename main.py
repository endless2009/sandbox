# -*- encoding:utf-8 -*-

from app import dispatch_apps,app,db
import sys
reload(sys)
sys.setdefaultencoding('utf8')

dispatch_apps(app)

if __name__ == '__main__':
    db.init_app(app)
    app.run(debug=True)
